const mysql = require('mysql2/promise');

async function getData () {
  const connection = await mysql.createConnection({
    host: process.env.DBHOST,
    user: process.env.DBUSER,
    password: process.env.DBPASSWORD,
    port: process.env.DBPORT
  });

  let res
  try {
    await connection.query('USE ' + process.env.DBNAME)

    const [rows] = await connection.query('SELECT * FROM students')

    res = rows.sort(function compare (a, b) {
      const calcA = JSON.parse(a.items).reduce(function (accumulator, currentValue, index, array) {
        return accumulator + currentValue.value
      }, 0)

      const calcB = JSON.parse(b.items).reduce(function (accumulator, currentValue, index, array) {
        return accumulator + currentValue.value
      }, 0)

      if (calcA < calcB) {
        return -1
      }
      if (calcA > calcB) {
        return 1
      }

      return 0
    }).pop()
  } catch (e) {
    console.log('Error', e)
  } finally {
    connection.end()
  }

  return res
}

getData()
  .then(x => {
    let res = ''
    res += 'Full name: '+ x.full_name + '\n'
    for (const item of JSON.parse(x.items)) {
      res +=`Item: ${item.title} - ${item.value}\n`
    }
    console.log(res)
    require('fs').writeFileSync('./result/data.txt', res, { encoding: 'utf8' })
  })
