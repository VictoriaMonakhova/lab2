# Calc

Run:

```bash
# install modules
npm ci

# run
node start
```

Example .env:

```dotenv
DBPORT=
DBHOST=
DBNAME=
DBUSER=
DBPASSWORD=
```
